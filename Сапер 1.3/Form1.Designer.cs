﻿namespace Сапер_1._3
{
    partial class Minesweeper
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Minesweeper));
            this.tileGrid = new Сапер_1._3.Minesweeper.TileGrid();
            this.gamebutton = new System.Windows.Forms.PictureBox();
            this.flagCounter = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuStripGame = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripGame_New = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuBreak1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStripExpert = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripBeginer = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStripIntermediate = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuBreak2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStripExit = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gamebutton)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileGrid
            // 
            resources.ApplyResources(this.tileGrid, "tileGrid");
            this.tileGrid.Name = "tileGrid";
            // 
            // gamebutton
            // 
            resources.ApplyResources(this.gamebutton, "gamebutton");
            this.gamebutton.BackgroundImage = global::Сапер_1._3.Properties.Resources.Game_Button;
            this.gamebutton.Name = "gamebutton";
            this.gamebutton.TabStop = false;
            this.gamebutton.Click += new System.EventHandler(this.LoadGame);
            // 
            // flagCounter
            // 
            resources.ApplyResources(this.flagCounter, "flagCounter");
            this.flagCounter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flagCounter.Name = "flagCounter";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStripGame});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // MenuStripGame
            // 
            this.MenuStripGame.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStripGame_New,
            this.MenuBreak1,
            this.MenuStripBeginer,
            this.MenuStripIntermediate,
            this.MenuStripExpert,
            this.MenuBreak2,
            this.MenuStripExit});
            this.MenuStripGame.Name = "MenuStripGame";
            resources.ApplyResources(this.MenuStripGame, "MenuStripGame");
            // 
            // MenuStripGame_New
            // 
            this.MenuStripGame_New.Name = "MenuStripGame_New";
            resources.ApplyResources(this.MenuStripGame_New, "MenuStripGame_New");
            this.MenuStripGame_New.Click += new System.EventHandler(this.MenuStripGame_New_Click);
            // 
            // MenuBreak1
            // 
            this.MenuBreak1.Name = "MenuBreak1";
            resources.ApplyResources(this.MenuBreak1, "MenuBreak1");
            // 
            // MenuStripExpert
            // 
            this.MenuStripExpert.Name = "MenuStripExpert";
            resources.ApplyResources(this.MenuStripExpert, "MenuStripExpert");
            this.MenuStripExpert.Tag = "expert";
            this.MenuStripExpert.Click += new System.EventHandler(this.MenuStrip_Game_DifficultyChanged);
            // 
            // MenuStripBeginer
            // 
            this.MenuStripBeginer.Name = "MenuStripBeginer";
            resources.ApplyResources(this.MenuStripBeginer, "MenuStripBeginer");
            this.MenuStripBeginer.Tag = "Beginner";
            this.MenuStripBeginer.Click += new System.EventHandler(this.MenuStrip_Game_DifficultyChanged);
            // 
            // MenuStripIntermediate
            // 
            this.MenuStripIntermediate.Name = "MenuStripIntermediate";
            resources.ApplyResources(this.MenuStripIntermediate, "MenuStripIntermediate");
            this.MenuStripIntermediate.Tag = "Intermediate";
            this.MenuStripIntermediate.Click += new System.EventHandler(this.MenuStrip_Game_DifficultyChanged);
            // 
            // MenuBreak2
            // 
            this.MenuBreak2.Name = "MenuBreak2";
            resources.ApplyResources(this.MenuBreak2, "MenuBreak2");
            // 
            // MenuStripExit
            // 
            this.MenuStripExit.Name = "MenuStripExit";
            resources.ApplyResources(this.MenuStripExit, "MenuStripExit");
            this.MenuStripExit.Click += new System.EventHandler(this.MenuStripExit_Click);
            // 
            // Minesweeper
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flagCounter);
            this.Controls.Add(this.tileGrid);
            this.Controls.Add(this.gamebutton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Minesweeper";
            ((System.ComponentModel.ISupportInitialize)(this.gamebutton)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TileGrid tileGrid;
        private System.Windows.Forms.PictureBox gamebutton;
        private System.Windows.Forms.Label flagCounter;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuStripGame;
        private System.Windows.Forms.ToolStripMenuItem MenuStripGame_New;
        private System.Windows.Forms.ToolStripMenuItem MenuStripBeginer;
        private System.Windows.Forms.ToolStripMenuItem MenuStripIntermediate;
        private System.Windows.Forms.ToolStripMenuItem MenuStripExpert;
        private System.Windows.Forms.ToolStripSeparator MenuBreak1;
        private System.Windows.Forms.ToolStripSeparator MenuBreak2;
        private System.Windows.Forms.ToolStripMenuItem MenuStripExit;
    }
}

